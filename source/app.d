import std.stdio;
import std.math : round;
import std.conv;

import dsfml.graphics;
import dsfml.system;
import global;
import neuralnet;

bool hold;

void handleEvent(Event e) {
	if(e.type == Event.EventType.KeyPressed) {
		hold = !hold;
	}
}

float[] inp;
float[] expected;
float[] actual;

void run() {
	while(window.isOpen()) {
		Event event;
		while (window.pollEvent(event)){
			handleEvent(event);
			// "close requested" event: we close the window
			if (event.type == Event.EventType.Closed){
					window.close();
				}
		}
		window.clear();
		if(!hold) {
			inp = [uniform!("[]")(0, 1, rnd), uniform!("[]")(0, 1, rnd)];
			expected = [to!int(round(inp[0]))^to!int(round(inp[1]))];
			actual = net.propagate(inp);
			net.propBackwards(inp, expected);
		}
		writeln(expected[0], ": ", actual[0]);
		net.display(cast(int)(width*0.08f), cast(int)(height*0.4f));
		
		
		window.display();
		frameCount++;
	}
}

void main()
{
	dsfmlInitWindow(window);
	net = new Network([2, 3, 1]);
	run();
}
