module utils;

import global;
import dsfml.system;
import dsfml.graphics;

///Function to iniialise the window
void dsfmlInitWindow(ref RenderWindow window) {
	window = new RenderWindow(VideoMode(cast(int)width, cast(int)height), //VideoMode.getFullscreenModes()[0],
		"Neural Network",
		Window.Style.None);
  	window.setFramerateLimit(60);
}

float sq(float x)
{
  import std.math : pow;

  return pow(x, 2);
}

@property float mag(Vector2f v)
{
  import std.math : sqrt;

  return sqrt(sq(v.x) + sq(v.y));
}

@property float dot(Vector2f f, Vector2f s)
{ //Dot product of vectors
  return f.x * s.x + f.y * s.y;
}

@property Vector2f mag(ref Vector2f f, float newmag)
{
  if (f.mag == 0)
    return f;
  f /= f.mag;
  f *= newmag;
  return f;
}

@property Vector2f rotate(ref Vector2f f, float ang)
{ //Takes ang in degrees
  import std.math : cos, sin, PI;

  float t = f.x;
  ang = ang / 180 * PI;
  f.x = f.x * cos(ang) - f.y * sin(ang);
  f.y = t * sin(ang) - f.y * cos(ang);
  return f;
}

void constrain(ref Vector2f f, float minx, float maxx, float miny, float maxy)
{
  if (f.x < minx)
    f.x = minx;
  if (f.x > maxx)
    f.x = maxx;
  if (f.y < miny)
    f.y = miny;
  if (f.y > maxy)
    f.y = maxy;
}

void text(string s, Vector2f pos, RenderStates st = RenderStates.Default()) {
  tex.setString(s);
  FloatRect textRect = tex.getLocalBounds();
  tex.origin = Vector2f(0, textRect.height/2);
  tex.position = pos;
  window.draw(tex, st);
}