module neuralnet;

import std.math : tanh, cosh;
import std.algorithm.comparison : min, max;
import std.stdio : writeln;
import std.conv : to;
import global;

import dsfml.graphics;
import dsfml.system;

class Network {
  Node[] inputs;
  Node[] outputs;

  //Display stuff
  float maxheight = 0;

  this(int[] lens)
  {
    foreach(i; 0..lens[0]) {
      maxheight = lens[0]>maxheight?lens[0]:maxheight;
      inputs ~= Node(lens[1]);
    }
    addNodes(inputs, lens[1..$]);
  }

  void addNodes(ref Node[] nodes, int[] lens)
  {
    Node[] temp;
    foreach(i; 0..lens[0]) {
      temp ~= Node(lens.length>1?lens[1]:0);
    }
    foreach(ref n; nodes) {
      if(n.next.length == 0) {
        n.next = temp;
        if(lens.length >= 2) {
          addNodes(n.next, lens[1..$]);
        }else{
          outputs = n.next;
        }
      }
    }

  }

  float[] propagate(float[] inp) {
    foreach(i, ref n; inputs) {
      n.val = inp[i];
      n.propagate();
    }
    float[] outs;
    foreach(ref n; outputs) {
      outs ~= n.val;
    }
    foreach(i, ref n; inputs) {
      n.reset(i==0);
    }
    return outs;
  }

  void propBackwards(float[] inp, float[] target) {
    foreach(i, t; target) {
      float marginError = t-outputs[i].disval;
      writeln("Error: ", marginError);
      //float dvaloutput = outputs.disval.deriv*marginError;
      foreach(j, ref n; inputs) {
        n.calcDeltas(j==0, marginError);
      }
      foreach(j, ref n; inputs) {
        n.backprop(j==0);
      }
    }
  }

  void display(int x, int y) {
    Transform t;
    t.translate(x, y+height*0.12f);
    //t.scale(1, 1/(maxheight*100)*height);
    //t.scale(Mouse.getPosition(window).x/500f, Mouse.getPosition(window).y/500f);
    RenderStates st = RenderStates();
    st.transform = t;
    foreach(i, ref n; inputs) {
      
      n.updatePos(0, 0, i, inputs.length, i==0);
      n.display(i==0, st);
      text(to!string(n.disval), n.pos+Vector2f(-50, 0), st);
    }
    foreach(ref n; outputs) {
      text(to!string(n.disval), n.pos+Vector2f(50, 0), st);
    }
  }
}

struct Node
{
  private float val = 0;
  float disval = 0;
  Node[] next;
  Synapse[] syns;
  Vector2f pos;
  float delta; //The error in this nodes value

  this(int len)
  {
    foreach(i; 0..len) {
      const float x = uniform(0.0f, 1.0f, rnd);
      syns ~= Synapse(x).update();
    }
  }

  void propagate(){
    foreach(i, ref n; next) {
      n.val += val*syns[i].val;
      n.val = n.val.activate();
      n.propagate();
    }
  }
  void calcDeltas(bool first, float error) {
    if(first) {
      foreach(i, ref n; next) {
        n.calcDeltas(i==0, error);
      }
    }
    delta = disval.deriv*error;
  }
  void backprop(bool first) {
    if(first) { //Call the next layer, should only be done once hence "first"
      foreach(i, ref n; next) {
        n.backprop(i==0);
      }
    }
    foreach(i; 0..syns.length) {
      syns[i].val += next[i].delta;
    }
  }
  void reset(bool first)
  {
    disval = val;
    val = 0;
    if(first) {
      foreach(i, ref n; next) {
        n.reset(i==0);
      }
    }
    
  }
  void updatePos(int x, int y, float i, float count, bool first) {
    pos = Vector2f(x, cast(float)y+(i-count/2)*100);
    if(first) { //Don't need to draw the next layer more than once.
      foreach(j, ref n; next) {
        n.updatePos(x+200, y,
          j,
          cast(int)next.length,
          j==0
        );
        
      }
    }
  }
  ///X position, Y position, Y offset, array to add this position, number of nodes in this layer
  void display(bool first, ref RenderStates st) {
    CircleShape c = new CircleShape(20);
    c.fillColor = Color.White;
    c.position = pos;
    c.origin = Vector2f(20, 20);
    window.draw(c, st);
    //text(to!string(disval), pos+Vector2f(0, 30), st); //Display the value
    VertexArray line = new VertexArray(PrimitiveType.Lines, 2);
    foreach(j, ref n; next) {
      syns[j].update(); //Update the hue of the synapse
      line.append(Vertex(pos, syns[j].col));
      line.append(Vertex(n.pos, syns[j].col));
      if(first) {
        n.display(j==0, st);
      }
      window.draw(line, st);
      line.clear();
    }
    
  }
}

struct Synapse {
  float val;
  Color col;

  Synapse update() {
    const float x = val; //H/S/B
    writeln(x);
    const float y = 1;
    const float z = 1;

    float which = (x-cast(int)x)*6.0f;
    float f = which-cast(int)which;
    float p = max(0, min(1, z*(1.0f - y)));
    float q = max(0, min(1, z*(1.0f - y*f)));
    float t = max(0, min(1, z*(1.0f - (y*(1.0f - f)))));
    switch(cast(int)which) {
      case 0: col.r = to!ubyte(z*255); col.g = to!ubyte(t*255); col.b = to!ubyte(p*255); break;
      case 1: col.r = to!ubyte(q*255); col.g = to!ubyte(z*255); col.b = to!ubyte(p*255); break;
      case 2: col.r = to!ubyte(p*255); col.g = to!ubyte(z*255); col.b = to!ubyte(t*255); break;
      case 3: col.r = to!ubyte(p*255); col.g = to!ubyte(q*255); col.b = to!ubyte(z*255); break;
      case 4: col.r = to!ubyte(t*255); col.g = to!ubyte(p*255); col.b = to!ubyte(z*255); break;
      case 5: col.r = to!ubyte(z*255); col.g = to!ubyte(p*255); col.b = to!ubyte(q*255); break;
      default: col = Color.Red;
    }

    // col.r = 255-h >> 24 & 0xFF;
    // col.g = 255-h >> 16 & 0xFF;
    // col.b = 255-h >>  8 & 0xFF;
    // col.a = 255;//max(100, h & 0xFF) & 0xFF;

    return this;
  }
}

@property float activate(float val) { //The activation function
  return tanh(val);
}
@property float deriv(float v) {
  return 1/cosh(v);
}
/*
void updatePos(int x, int y, float i, float count, bool first) {
    pos = Vector2f(x, cast(float)y+(i-count/2)*100);
    if(first) { //Don't need to draw the next layer more than once.
      foreach(j, ref n; next) {
        n.updatePos(x+200, y,
          j,
          cast(int)next.length,
          j==0
        );
        
      }
    }
  }
  ///X position, Y position, Y offset, array to add this position, number of nodes in this layer
  void display(ref Vector2f[] coords, bool first) {
    coords ~= pos;
    if(first) { //Don't need to draw the next layer more than once.
      foreach(j, ref n; next) {
        n.display(
          coords,
          j==0
        );
        
      }
    }
  }*/