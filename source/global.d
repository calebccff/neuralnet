module global;

public import std.random;
public import utils;

import dsfml.graphics;
RenderWindow window;


int
  width = 800,
  height = 500,
  frameCount = 0;

Random rnd;
Font _font;
Text tex;

import neuralnet;
Network net;

static this()
{
  rnd = Random(unpredictableSeed);
  _font = new Font();
  if (!_font.loadFromFile("/usr/share/fonts/TTF/TerminusTTF.ttf"))
  {
    import std.stdio : writeln;
    writeln("Couldn't find font, no text will be displayed");
  }
  tex = new Text();
  tex.setFont(_font);
  tex.setColor(Color.Blue);
  tex.setCharacterSize(22);
}
